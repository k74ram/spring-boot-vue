package id.org.kslbl.spring.springbootvue.repository;

import id.org.kslbl.spring.springbootvue.entity.Todo;

import org.springframework.data.jpa.repository.JpaRepository;  
import org.springframework.data.rest.core.annotation.RepositoryRestResource;  
  
@RepositoryRestResource  
public interface TodoRepository extends JpaRepository<Todo, Long> {}
